.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

.. _introduction:

------------
Introduction
------------

White Rabbit Trigger Distribution (WRTD) is a generic framework for distributing triggers (Events)
over a White Rabbit (WR) network.

As can be seen in :numref:`fig-wrtd-overview`, WRTD Nodes receive "input" Events and distribute them
to other Nodes over WR in the form of network Messages that are used to transfer the Timestamp of
the input Event. The receiving Nodes are programmed to execute some "output" Event (action) upon
reception of a particular Message, potentially with some fixed delay added to the Timestamp.

.. figure:: graphics/wrtd_overview.png
   :name: fig-wrtd-overview
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   Overview of WRTD

There are two main categories of WRTD applications:

#. A "source" Node receives an input Event, adds a fixed delay to its Timestamp and distributes it
   to other Nodes. As long as the fixed delay added is greater than the upper-bound latency of the
   network (a fundamental feature of WR itself), all receiving nodes will receive the Message before
   the programmed time and will execute simultaneously their action (thanks to the sub-ns
   synchronisation provided by WR).

#. The receiving Nodes are "recording" devices (e.g. digitisers), capable of storing data in a
   recording buffer. The source Node transmits the Message, with or without a fixed delay. When one
   of the destination Nodes receives the Message, it stops recording and rolls-back its buffer to
   the moment specified by the Timestamp in the received Message (provided that it has a large
   enough buffer to compensate for the latency). Thus, all Nodes will deliver recorded data from the
   moment in the past when the input Event was originally received at the source Node.

Of course, the above list is not exhaustive, there are many other potential applications but they
are usually permutations of one of the above scenarios.

In WRTD, the programming of Events, Messages and associated actions is done by defining Rules. A
Rule simply declares a relationship between an input (cause) and an output (effect) Event. A Rule
can state that when a specific Event is received a Message should be transmitted or, that when a
Message is received an output Event should be generated. This is depicted in
:numref:`fig-wrtd-node`.

.. figure:: graphics/wrtd_node_basic.png
   :name: fig-wrtd-node
   :width: 300pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   Inside a WRTD Node

:numref:`basic_concepts` provides a more elaborate discussion on the various basic concepts of WRTD.

.. _lxi:

Relation to IVI and LXI
=======================

`LAN eXtensions for Instrumentation (LXI) <http://www.lxistandard.org>`_ is a standard, defining a
communication protocol for the remote control of instrumentation over an Ethernet-based LAN.

Version 1.5 of the LXI standard splits the specification in two parts:

#. The `LXI Core Specification`_, to which all LXI Devices must conform.

#. A set of optional Extended Functions, to which vendors may choose to conform.

Among these "Extended Functions", several are related to the synchronisation (via IEEE-1588 PTP) and
exchange of real-time event messages between instruments. These include:

* `LXI Event Log Extended Function`_
* `LXI Timestamped Data Extended Function`_
* `LXI Clock Synchronization Extended Function`_
* `LXI Event Messaging Extended Function`_

The core specification requires (Rule 6.1) that all LXI devices provide an `Interchangeable Virtual
Instruments (IVI) <http://ivifoundation.org/>`_ driver. Furthermore, it requires (Rule 6.1.1) that
all LXI devices supporting the exchange of event messages, do so by providing an API that conforms
to the `IVI-3.15 IviLxiSync Specification`_.

Since the LXI event exchanging mechanism is conceptually very close to WRTD, it was decided to
design WRTD to be as close to LXI as possible. In particular:

* WRTD uses the same Message format. This already allows LXI and WRTD devices on the same network to
  exchange events, even if the API for programming these events is different.
* the WRTD library API mimics that of an IVI driver, with a strong influence from the `IVI-3.15
  IviLxiSync Specification`_, even if several of the Repeated Capabilities and Attributes are
  different.

.. hint:: Do not worry if you do not understand some of the terminology yet. It will be explained in
          :numref:`basic_concepts`.

In the future, and with `White Rabbit standardised within IEEE-1588-2019
<https://www.ohwr.org/project/wr-std/wikis/home>`_, it is foreseen to try to merge WRTD with
IVI/LXI. A possible way to do this would be to add a new IVI specification, similar to IVI-3.15,
describing the API to control WRTD-enabled devices. This API would be an extension, allowing any
instrument with an IVI driver and a WR interface to exchange event messages with any other WRTD
node.

Document Structure
==================

The following is a description of how the remainder of this document is structured (in reverse
order).

* :numref:`devel` provides guidelines on how to develop a WRTD Node, including hardware
  (:numref:`hw_dev`), gateware (:numref:`gw_dev`) and firmware (:numref:`fw_dev`).

* Before you embark however on a new design, please have a look first at the existing reference
  designs; it could be that one of them is appropriate for your task. :numref:`ref_nodes` presents
  the currently available reference WRTD Nodes that come pre-programmed with their gateware and
  firmware.

* Whether you develop your own Node or use one of the reference Nodes, :numref:`usage` describes how
  to access and control your Node. :numref:`clib` provides all the details on how to use the C
  library to develop your own applications. Alternatively, :numref:`pywrap` presents a Python
  wrapper to the C library that can be used to develop Python-based applications. :numref:`tools`
  describes the generic tools that are built using the Python wrapper and that provide access to a
  WRTD node without the need to develop any application.

* :numref:`basic_concepts` introduces the various basic concepts of WRTD. These concepts are used
  throughout this document and are fundamental to understanding how WRTD works (and, by extension,
  how to use it).

* Last but not least, :numref:`installation` takes you through the necessary steps to setup the
  hardware and install the necessary software.

Documentation License
=====================

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

.. _LXI Core Specification: http://www.lxistandard.org/members/Adopted%20Specifications/Latest%20Version%20of%20Standards_/LXI%20Standard%201.5%20Specifications/LXI%20Device%20Specification%20v1_5_01.pdf

.. _LXI Event Log Extended Function: http://www.lxistandard.org/members/Adopted%20Specifications/Latest%20Version%20of%20Standards_/LXI%20Standard%201.5%20Specifications/LXI%20Event%20Log%20Extended%20Function.pdf

.. _LXI Timestamped Data Extended Function: http://www.lxistandard.org/members/Adopted%20Specifications/Latest%20Version%20of%20Standards_/LXI%20Standard%201.5%20Specifications/LXI%20Timestamped%20Data%20Extended%20Function.pdf

.. _LXI Clock Synchronization Extended Function: http://www.lxistandard.org/members/Adopted%20Specifications/Latest%20Version%20of%20Standards_/LXI%20Standard%201.5%20Specifications/LXI%20Clock%20Synchronization%20Extended%20Function.pdf

.. _LXI Event Messaging Extended Function: http://www.lxistandard.org/members/Adopted%20Specifications/Latest%20Version%20of%20Standards_/LXI%20Standard%201.5%20Specifications/LXI%20Event%20Messaging%20Extended%20Function.pdf

.. _IVI-3.15 IviLxiSync Specification: http://www.ivifoundation.org/downloads/Architecture%20Specifications/IVI-3.15_LxiSync_2018-08-23.pdf

.. White Rabbit Trigger Distribution documentation master file, created by
   sphinx-quickstart on Fri Jan 25 09:42:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

===============================================
White Rabbit Trigger Distribution documentation
===============================================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Table of Contents

   introduction
   installation
   basic_concepts
   usage_index
   ref_nodes
   devel

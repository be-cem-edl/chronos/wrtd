.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

.. _usage:

-----
Usage
-----

There are three options offered for accessing a WRTD Node (in decreasing order of flexibility and complexity):

* By writing your own application using the provided :ref:`clib`.
* By writing your own application using the provided :ref:`pywrap`.
* By using the provided :ref:`tools`.

.. toctree::
   :hidden:

   usage_clib
   usage_pywrap
   usage_tools

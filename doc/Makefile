# SPDX-FileCopyrightText: 2022 CERN
#
# SPDX-License-Identifier: CC-BY-SA-4.0+

# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = wrtd
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help doxygen Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	$(MAKE) doxygen TARGET=$@
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


GIT_VERSION = $(shell git describe --always --dirty --long --tags)

doxygen:
ifeq ($(TARGET),clean)
	@rm -rf _build doxygen-wrtd-output .doxystamp
else
	$(MAKE) .doxystamp
endif

# List of Doxygen folders to consider
DOXINPUT := ../software/lib
DOXINPUT += ../software/include
DOXINPUT += ../software/firmware/include
DOXEXCL  :=  ""

# List of actual Doxygen source files
DOXSRC   = $(shell find $(DOXINPUT) -type f -name '*.[chS]')

.doxystamp: $(DOXSRC)
	GIT_VERSION=$(GIT_VERSION) DOXINPUT="$(DOXINPUT)" DOXEXCL="$(DOXEXCL)" doxygen ./doxygen-wrtd-config
	@touch .doxystamp

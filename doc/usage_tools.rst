.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

.. _tools:

Tools
=====

WRTD provides a comand-line, Python based tool (:ref:`wrtd_tool`) for accessing a :ref:`node`.

.. hint::

   Please make sure that you run the tool wih the proper :ref:`permissions`.

For details on how to install the tool (and their dependencies), please refer to
:numref:`installation`.

.. _wrtd_tool:

wrtd-tool
---------

``wrtd-tool`` is a command-line tool that implements several different operations on a
:ref:`node`. It supports most of the functionality provided by the :ref:`pywrap`.

A list of the available commands can be retrieved by passing the ``-h`` option to the tool:

.. code-block:: console

   > wrtd-tool -h
   usage: wrtd-tool [-h] <command> ...

   WRTD Node configuration tool

   optional arguments:
     -h, --help            show this help message and exit

   Available commands:
     <command>             (Use "<command> -h" to get more details)
       list-nodes          List the IDs of all detected WRTD Nodes
       sys-info            Show system information
       sys-time            Show current system time
       enable-log          Enable logging
       disable-log         Disable logging
       show-log            Show log entries
       clear-log           Clear pending log entries
       list-rules          List all defined Rules
       add-rule            Define a new Rule
       set-rule            Configure a Rule
       remove-rule         Delete a Rule
       remove-all-rules    Delete all Rules
       enable-rule         Enable a Rule
       disable-rule        Disable a Rule
       disable-all-rules   Disable all Rules
       rule-info           Display information about a Rule
       reset-rule-stats    Reset all statistics of a Rule
       list-alarms         List all defined Alarms
       add-alarm           Define a new Alarm
       set-alarm           Configure an Alarm
       remove-alarm        Delete an Alarm
       remove-all-alarms   Delete all Alarms
       enable-alarm        Enable an Alarm
       disable-alarm       Disable an Alarm
       disable-all-alarms  Disable all Alarms
       alarm-info          Display information about an Alarm
       cli                 Command line interface

Each command has its own built-in help system as well, which can be invoked by selecting a command
and passing the ``-h`` option after the command:

.. code-block:: console

   > wrtd-tool set-alarm -h
   usage: wrtd-tool set-alarm [-h] -d DELAY [-s SETUP] [-p PERIOD]
                              [-c COUNT] [-e] <node_id> <alarm_id>

   positional arguments:
     <node_id>             The ID of the WRTD Node (int, can be hex with "0x"
                           prefix)
     <alarm_id>            The ID of the Alarm (string up to 15 characters)

   optional arguments:
     -h, --help            show this help message and exit
     -d DELAY, --delay DELAY
                           Set the delay for this Alarm wrt now. Default
                           unit is ns, but an 'n','u','m' or 's' can be
                           appended to the value to set it explicitly to
                           nano, micro, milli or full seconds
     -s SETUP, --setup SETUP
                           Set the setup time for this Alarm. Default
                           unit is ns, but an 'n','u','m' or 's' can be
                           appended to the value to set it explicitly to
                           nano, micro, milli or full seconds
     -p PERIOD, --period PERIOD
                           Set the period for this Alarm. Default unit
                           is ns, but an 'n','u','m' or 's' can be
                           appended to the value to set it explicitly to
                           nano, micro, milli or full
     -c COUNT, --count COUNT
                           Set the repeat count for this Alarm
     -e, --enable          Also enable the Alarm after configuring it.

If a command returns an :ref:`Error Code <api_error_codes>`, the underlying :ref:`pywrap` will raise
an OSError exception and will provide all the available details:

.. code-block:: console

   > wrtd-tool remove-alarm 1 alarm5
   OSError: [Errno -1074122744] WRTD_ERROR_ALARM_DOES_NOT_EXIST
            wrtd_remove_alarm/wrtd_find_alarm: The specified alarm has not been defined

Here's an example on how to configure a :ref:`rule` and check the :ref:`event_log` for :ref:`Events
<event>`:

.. code-block:: console

   > wrtd-tool list-nodes
   -> WRTD Node detected with ID: 10
   > wrtd-tool list-rules 10
   0 Rules defined.
   > wrtd-tool add-rule 10 rule0
   > wrtd-tool set-rule 10 rule0 LC-I1 NET0
   > wrtd-tool list-rules 10 -v
   1 Rule defined:
   + rule0
     + Configuration
       - Name..............: rule0
       - Source............: LC-I1
       - Destination.......: NET0
       - Enabled...........: False
       - Send Late.........: True
       - Repeat Count......: 0
       - Delay.............: 0.000ns
       - Holdoff...........: 0.000ns
       - Resync Period.....: 0.000ns
       - Resync Factor.....: 0
     + Statistics
       - RX Events.........: 0
       - Last RX...........: Never
       - TX Events.........: 0
       - Last TX...........: Never
       - Latency (min).....: 0.000ns
       - Latency (avg).....: 0.000ns
       - Latency (max).....: 0.000ns
       - Missed (late).....: 0
       - Missed (holdoff)..: 0
       - Missed (no sync)..: 0
       - Missed (overflow).: 0
       - Last Missed.......: Never
   > wrtd-tool enable-rule 10 rule0
   > wrtd-tool enable-log 10
   > wrtd-tool show-log 10 -c 6
   Id:LC-I1           |Seq:0000|...|...|GENERATED|DEVICE_0
   Id:NET0            |Seq:0016|...|...|NETWORK  |TX
   Id:LC-I1           |Seq:0000|...|...|GENERATED|DEVICE_0
   Id:NET0            |Seq:0017|...|...|TX
   Id:LC-I1           |Seq:0000|...|...|GENERATED|DEVICE_0
   Id:NET0            |Seq:0018|...|...|TX

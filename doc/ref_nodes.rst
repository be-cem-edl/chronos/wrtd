.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

.. _ref_nodes:

---------------
Reference Nodes
---------------

See repository https://ohwr.org/project/wrtd-ref-designs/ for some reference designs.

.. Copyright (c) 2019 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

.. _installation:

------------
Installation
------------

.. note:: Because at the time of the release of WRTD v1.0, the OHWR deployment procedures were
          undergoing significant changes (in particular with respect to packaging), up-to-date
          installation instructions will be available through the `project Wiki
          <https://www.ohwr.org/project/wrtd/wikis/Installation-Instructions>`_. Once these
          procedures have been finalised and tested, the contents of the wiki page will be merged
          here.

.. _permissions:

Permissions
===========

By default, WRTD installs with read/write permissions for the root account only.

For users to be able to access the Nodes with a non-root account, it is necessary to set
appropriately the file permissions of all devices under ``/dev/mockturtle``.

Note that this is handled automatically when WRTD is installed through packages.

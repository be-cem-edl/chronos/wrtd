# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

from setuptools import setup, find_packages

import os

setup(
    name="wrtd-tool",
    version=os.environ.get('VERSION', "0.0.0"),
    packages=find_packages(),
    description='WRTD Tool',
    author="Dimitris Lampridis",
    author_email="dimitris.lampridis@cern.ch",
    maintainer="Tristan Gingold",
    maintainer_email="tristan.gingold@cern.ch",
    url='http://www.ohwr.org/projects/wrtd',
    project_urls={
        "Documentation": "https://be-cem-edl.web.cern.ch/wrtd/",
        "Forum"        : "https://forums.ohwr.org/c/wrtd",
    },
    license='LGPLv2.1',
    entry_points = {
        'console_scripts': [
            "wrtd-tool = wrtd_tool.wrtd_tool:main"
        ]
    }
)

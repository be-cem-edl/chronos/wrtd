#!/usr/bin/env python
"""
@copyright: Copyright (c) 2019 CERN (home.cern)

SPDX-License-Identifier: LGPL-2.1-or-later
"""
from setuptools import setup, find_packages

import os

setup(
    name='PyWrtd',
    version=os.environ.get('VERSION', "0.0.0"),
    description='Python wrapper for WRTD',
    author='Milosz Malczak',
    author_email='milosz.malczak@cern.ch',
    maintainer="Tristan Gingold",
    maintainer_email="tristan.gingold@cern.ch",
    url='http://www.ohwr.org/projects/wrtd',
    project_urls={
        "Documentation": "https://be-cem-edl.web.cern.ch/wrtd/",
        "Forum"        : "https://forums.ohwr.org/c/wrtd",
    },
    install_requires=['decorator'],
    packages=find_packages(),
    license='LGPLv2.1',
)

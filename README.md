<!--
SPDX-FileCopyrightText: 2022 CERN

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

# White Rabbit Trigger Distribution - WRTD

The aim of the project is to provide a system that is able to receive triggers
from some external source and distribute these triggers to remote nodes over
the White Rabbit network.

For more information, please refer to:

* [The project Wiki](https://ohwr.org/project/wrtd/wikis/Home)
* [Forum](https://forums.ohwr.org/c/wrtd)

## Licensing

* HDL simulations released under `CERN-OHL-W-2.0`
* All software released under `LGPL-2.1-or-later`
* Documentation released under `CC-BY-SA-4.0+`

Plain text copies of these licenses are available under the 'LICENSES' folder.

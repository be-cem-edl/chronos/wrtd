// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+
//------------------------------------------------------------------------------
// White Rabbit Trigger Distribution (WRTD)
// https://ohwr.org/projects/wrtd
//------------------------------------------------------------------------------
//
// unit name:   WrtdAlarm
//
// description: A SystemVerilog Class for a WRTD "alarm" repeated capability
//------------------------------------------------------------------------------

`ifndef __WRTD_ALARM_INCLUDED
 `define __WRTD_ALARM_INCLUDED

 `include "wrtd_rep_cap.svh"

class WrtdAlarm extends WrtdRepCap;
   protected WrtdTstamp setup_time;
   protected wrtd_event ev;
   protected int        repeat_count;
   protected uint32_t   period_ns;

   function new ( string name = "" );
      super.new ( name );
      this.setup_time = new();
      this.ev.ts = new();
      this.ev.id = new();
      clear();
   endfunction // new

   function void clear ( );
      super.clear();
      this.setup_time.zero();
      this.ev.ts.zero();
      this.ev.id.clear();
      this.ev.seq       = 0;
      this.ev.flags     = 0;
      this.repeat_count = 0;
      this.period_ns    = 0;
   endfunction // clear

   function wrtd_data data_pack ( );
      wrtd_data ret = new[`WRTD_ALRM_WORD_SIZE];
      ret[0:2] = this.setup_time.data_pack();
      ret[3:5] = this.ev.ts.data_pack();
      ret[6:9] = this.ev.id.data_pack();
      ret[10]  = this.ev.seq;
      ret[11]  = this.ev.flags;
      ret[12]  = this.enabled;
      ret[13]  = this.repeat_count;
      ret[14]  = this.period_ns;
      return ret;
   endfunction // data_pack

   function void data_unpack ( wrtd_data data );
      this.setup_time.data_unpack ( data[0:2] );
      this.ev.ts.data_unpack ( data[3:5] );
      this.ev.id.data_unpack ( data[6:9] );
      this.ev.seq       = data[10];
      this.ev.flags     = data[11];
      this.enabled      = data[12];
      this.repeat_count = data[13];
      this.period_ns    = data[14];
   endfunction // data_unpack

endclass //WrtdAlarm

`endif //  `ifndef __WRTD_ALARM_INCLUDED

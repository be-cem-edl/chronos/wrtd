<!--
SPDX-FileCopyrightText: 2022 CERN

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2023-11-14

### Changed
- sw/py: update packaging and versioning (#15)
- bld: use common EDL CI pipelines
- update MockTurtle to v4.3.0
- bld: improve MockTurtle building and linking (#17)

### Fixed
- sw: fixed reporting of HMQ sizes (#14)

### Removed
- sw: remove unused 'flags' field from events and messages (#16)

## [1.1.1] - 2023-05-22
### Added
- fw: wrtd-rt-common.h: added option to use VLAN

## [1.1.0] - 2023-01-16
### Added
- sw: wrtd_tool.py: add a cli command
### Removed
- sw: hdl_gen_tool removed, was never finished/ready for use
### Changed
- All reference designs have been moved to a separate repository, this project now
  only contains the WRTD core
- Updated MockTurtle to v4.1.0
- sw: Better python packaging, including wrtd-tool
- sw: Implemented reset argument in wrtd_init()
### Fixed
- fw: rule stats are kept when a rule is moved
- sw: bug for channels with more than one digit (e.g. LC-I11)
- sw: auto-wrap ns timestamp at 1e9
- sw: provide WRTD_ERR_MSG_BUF_SIZE to avoid hard-coding max err message buffer size
- sw: bug in remove_all_rules causing crashes
- sw: rules can no longer be enabled if source/destination id is invalid

## [1.0.1] - 2021-02-10
### Fixed
- add default interpreter for wrtd-tool

## [1.0.0] - 2019-10-01
### Added
- First release of WRTD.
